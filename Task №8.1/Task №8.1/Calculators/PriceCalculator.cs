﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Pets;

namespace Task__8._1.Calculators
{           
    public class PriceCalculator
    {        
        private ICalculateStrategy[] _concreteCalculators;

        public PriceCalculator(ICalculateStrategy[] concreteCalculators)
        {
            _concreteCalculators = concreteCalculators;
        }

        public int CalculatorPrice(Pet pet)
        {
            foreach (var concreteCalculator in _concreteCalculators)
            {
                if (concreteCalculator.IsSuitable(pet))
                {
                    return concreteCalculator.CalculatePrice(pet);
                }
            }
            throw new ArgumentException("Неизвестное животное");
        }
        public DateTime TimeBuy()
        {
            var random = new Random();
            var queue = random.Next(5, 20);
            return DateTime.UtcNow.AddMinutes(queue);
        }
    }
}
