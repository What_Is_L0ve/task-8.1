﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Pets;

namespace Task__8._1.Calculators
{
    public class DogCalculator : ICalculateStrategy
    {
        public int CalculatePrice(Pet pet)
            => 150 + 70 * pet.Coefficient;

        public bool IsSuitable(Pet pet)
            => pet is Dog;
    }
}
