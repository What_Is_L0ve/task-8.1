﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Pets;

namespace Task__8._1.Calculators
{
    public class CatCalculator : ICalculateStrategy
    {
        public int CalculatePrice(Pet pet)
            => 100 + 50 * pet.Coefficient;

        public bool IsSuitable(Pet pet)
            => pet is Cat;
    }
}
