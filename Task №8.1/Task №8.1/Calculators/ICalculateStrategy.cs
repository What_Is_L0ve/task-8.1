﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Pets;

namespace Task__8._1.Calculators
{
    public interface ICalculateStrategy
    {
        public bool IsSuitable(Pet pet);
        public int CalculatePrice(Pet pet);
    }
}
