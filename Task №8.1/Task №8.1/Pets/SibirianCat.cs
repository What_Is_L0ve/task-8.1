﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public class SibirianCat : Cat
    {
        public SibirianCat(string name)
            : base(name)
        {
        }
        public override int Coefficient => 4;
    }
}
