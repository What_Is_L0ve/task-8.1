﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public class AkitaDog : Dog
    {
        public AkitaDog(string name) 
            : base(name)
        {
        }
        public override int Coefficient => 2;
    }
}
