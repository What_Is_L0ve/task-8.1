﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public class AbyssinianCat : Cat
    {
        public AbyssinianCat(string name)
            : base(name)
        {
        }
        public override int Coefficient => 1;
    }
}
