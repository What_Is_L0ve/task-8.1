﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public abstract class Dog : Pet
    {
        protected Dog(string name)
            : base(name)
        {
        }
    }
}
