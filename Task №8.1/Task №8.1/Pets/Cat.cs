﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public abstract class Cat : Pet
    {
        protected Cat(string name)
            : base(name)
        {
        }
    }
}
