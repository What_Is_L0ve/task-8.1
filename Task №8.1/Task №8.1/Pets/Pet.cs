﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public abstract class Pet
    {
        public string Name { get; }
        protected Pet(string name)
            => Name = name;
        public abstract int Coefficient { get; }
        public override string ToString()
            => Name;
    }
}
