﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1.Pets
{
    public class BritishCat : Cat
    {
        public BritishCat(string name)
            : base(name)
        {
        }
        public override int Coefficient => 2;
    }
}
