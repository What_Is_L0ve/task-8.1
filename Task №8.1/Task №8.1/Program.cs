﻿using System;
using Task__8._1.Calculators;

namespace Task__8._1
{
    public class Program
    {        
        static void Main(string[] args)
        {            
            var priceCalculator = new PriceCalculator(new ICalculateStrategy[]
            {
                new DogCalculator(),
                new CatCalculator()
            });

            var clientStorage = new ClientStorage();
            var studio = new Studio(priceCalculator, clientStorage);

            var clients = new NewClient[]
            {
                new NewClient("Артур","89123098332", new Pets.AbyssinianCat("Кузя")),
                new NewClient("Артур","89123098332", new Pets.AkitaDog("Хатико")),
                new NewClient("Андрей","89439989321", new Pets.BritishCat("Феликс")),
                new NewClient("Алена","890921343567", new Pets.AlabaiDog("Лаки")),
                new NewClient("Максим","89325647752", new Pets.SibirianCat("Том")),
                new NewClient("Максим","89325647752", new Pets.HuskiesDog("Вольт"))
            };

            foreach (var client in clients)
            {
                Console.WriteLine($"Поступил заказ от {client} на стрижку питомца {client.Pet.Name}");
                Console.WriteLine($"Порода: {client.Pet.GetType().Name}");
                Console.WriteLine($"Сложность стрижки: {client.Pet.Coefficient}");
                Console.WriteLine($"Цена: {priceCalculator.CalculatorPrice(client.Pet)} Время завершения: {priceCalculator.TimeBuy()}\n");
                studio.HandleClient(client);
            }
            Console.WriteLine();
            foreach (var client in clientStorage.Clients)
            {
                Console.WriteLine(client);
            }
        }
    }
}
