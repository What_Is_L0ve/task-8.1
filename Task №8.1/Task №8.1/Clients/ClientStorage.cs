﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1
{
    public class ClientStorage
    {
        private StoragedClient[] _clients;
        public StoragedClient[] Clients => _clients;

        public ClientStorage()
        {
            _clients = new StoragedClient[] { };
        }

        public void AddOrUpdate(NewClient newClient)
        {
            var storagedClient = Get(newClient);
            if(storagedClient == null)
            {
                Array.Resize(ref _clients, _clients.Length + 1);
                var newStoragedClient = new StoragedClient(newClient.Name, newClient.Phone);
                _clients[_clients.Length - 1] = newStoragedClient;
                newStoragedClient.AddPet(newClient.Pet);
                return;
            }
            storagedClient.AddPet(newClient.Pet);
        }

        private StoragedClient Get(Client client)
        {
            foreach (var storagedClient in _clients)
            {
                if (client.Name == storagedClient.Name && client.Phone == storagedClient.Phone)
                    return storagedClient;
            }
            return null;
        }
    }
}
