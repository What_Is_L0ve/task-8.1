﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1
{
    public class NewClient : Client
    {
        public NewClient(string name, string phone, Pets.Pet pet)
            : base(name, phone)
            => Pet = pet;
        
        public Pets.Pet Pet { get; }
    }
}
