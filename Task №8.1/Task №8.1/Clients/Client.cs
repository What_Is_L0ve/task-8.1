﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Pets;

namespace Task__8._1
{
    public abstract class Client
    {
        protected Client(string name, string phone)
        {
            Name = name;
            Phone = phone;            
        }
        public string Name { get; }
        public string Phone { get; }        
        public override string ToString() 
            => $"{Name} Номер: {Phone}";
    }
}
