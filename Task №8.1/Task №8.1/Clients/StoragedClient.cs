﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__8._1
{
    public class StoragedClient : Client
    {
        private Pets.Pet[] _pets;

        public StoragedClient(string name, string phone)
            : base(name, phone)
        {
            _pets = new Pets.Pet[] { };
        }

        public void AddPet(Pets.Pet pet)
        {
            Array.Resize(ref _pets, _pets.Length + 1);
            _pets[_pets.Length - 1] = pet;
        }

        public bool Contains(Pets.Pet pet)
        {
            foreach (var oldPet in _pets)
            {
                if (pet.GetType() == oldPet.GetType() && pet.Name == oldPet.Name)
                    return true;
            }
            return false;
        }
    }
}
