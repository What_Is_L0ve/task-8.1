﻿using System;
using System.Collections.Generic;
using System.Text;
using Task__8._1.Calculators;

namespace Task__8._1
{
    public class Studio
    {
        private ClientStorage _clientStorage;
        private Calculators.PriceCalculator _priceCalculator;

        public Studio(PriceCalculator priceCalculator, ClientStorage clientStorage)
        {
            _priceCalculator = priceCalculator;
            _clientStorage = clientStorage;
        }
        public int HandleClient(NewClient newClient)
        {
            _clientStorage.AddOrUpdate(newClient);
            return _priceCalculator.CalculatorPrice(newClient.Pet);
        }
    }
}
